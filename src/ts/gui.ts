// notie
import * as notie from "notie";
import "notie/dist/notie.css";
// fontawesome
import "@fortawesome/fontawesome-free/css/all.css";
// open-sans
import "@fontsource/open-sans";
// basthon
import { GUIBase, GUIOptions } from "@basthon/gui-base";
// local
import Shell from "./shell";
import Editor from "./editor";
import Graphics from "./graphics";
import "../css/style.css";

export default class GUI extends GUIBase {
  private _shell: Shell;
  private _editor: Editor;
  private _graphics: Graphics;

  public constructor(options: GUIOptions) {
    super(
      (() => {
        options.uiName = "console";
        return options;
      })()
    );
    this._urlKey = "script";
    this._contentFilename = "script";
    this._shell = new Shell(this);
    this._editor = new Editor(this);
    this._graphics = new Graphics(this);
    /* when kernel is available, set contentFilename extension */
    this.kernelLoader.kernelAvailable().then((kernel) => {
      const exts = kernel.moduleExts();
      if (exts.length) this._contentFilename += "." + exts[0];
    });
  }

  /**
   * Get editor's content.
   */
  public content(): string {
    return this._editor.getContent();
  }

  /**
   * Set editor's content.
   */
  public setContent(content: string): void {
    this._editor.setContent(content);
  }

  /**
   * Editor getter.
   */
  public get editor() {
    return this._editor;
  }

  /**
   * Shell getter.
   */
  public get shell() {
    return this._shell;
  }

  /**
   * Graphics getter.
   */
  public get graphics() {
    return this._graphics;
  }

  /**
   * Notify the user with an error.
   */
  public error(title: string, message: string) {
    notie.alert({
      type: "error",
      text: message,
      stay: false,
      time: 3,
      position: "top",
    });
  }

  /**
   * Notify the user.
   */
  public info(title: string, message: string) {
    notie.alert({
      type: "success",
      text: message,
      stay: false,
      time: 3,
      position: "top",
    });
  }

  /**
   * Ask the user to confirm or cancel.
   */
  public confirm(
    title: string,
    message: string,
    text: string,
    callback: () => void,
    textCancel: string,
    callbackCancel: () => void
  ): void {
    notie.confirm({
      text: message,
      submitText: text,
      cancelText: textCancel,
      position: "top",
      submitCallback: callback,
      cancelCallback: callbackCancel,
    });
  }

  /**
   * Ask the user to select a choice.
   */
  public select(
    title: string,
    message: string,
    choices: {
      text: string;
      handler: () => void;
    }[],
    textCancel: string,
    callbackCancel: () => void
  ) {
    let count = 0;
    notie.select({
      text: message,
      cancelText: textCancel,
      position: "top",
      choices: choices.map((c) => ({
        text: c.text,
        type: count++ % 2 ? "error" : "success",
        handler: c.handler || (() => {}),
      })),
      cancelCallback: callbackCancel || (() => {}),
    });
  }

  protected async setupUI(options: any) {
    await super.setupUI(options);

    /* update appearance from state */
    await this.updateSwitchView();
    await this.updateDarkLight();

    await Promise.all([
      this._editor.init(),
      this._shell.init(),
      this._graphics.init(),
    ]);

    /* connecting buttons */
    // run
    let button = document.getElementById("run");
    button?.addEventListener("click", () => this.runScript());
    // open
    button = document.getElementById("open");
    button?.addEventListener("click", () => this.openFile());
    // download
    button = document.getElementById("download");
    button?.addEventListener("click", () => this.download());
    // share
    button = document.getElementById("share");
    button?.addEventListener("click", () => this.share());
    // rolling back to previous version
    button = document.getElementById("rollback");
    button?.addEventListener("click", async () => {
      const content = await this.selectCheckpoint();
      if (content != null) this.setContent(content);
    });

    // raz
    button = document.getElementById("raz");
    button?.addEventListener("click", () => this.restart());
    // show shell
    button = document.getElementById("btn_shell");
    button?.addEventListener("click", () => this.showShell());
    // show graph
    button = document.getElementById("btn_graph_view");
    button?.addEventListener("click", () => this.showGraph());
    // dark/light mode
    button = document.getElementById("darklight");
    button?.addEventListener("click", () => this.switchDarkLight());
    // view switch
    button = document.getElementById("switch");
    button?.addEventListener("click", () => this.switchView());
    // single/double vew
    button = document.getElementById("hide-console");
    button?.addEventListener("click", () => this.hideConsole());
    button = document.getElementById("hide-editor");
    button?.addEventListener("click", () => this.hideEditor());
    button = document.getElementById("show-editor-console");
    button?.addEventListener("click", () => this.showEditorConsole());

    /* binding ctrl+s */
    window.addEventListener("keydown", (event) => {
      if (
        (event.ctrlKey || event.metaKey) &&
        String.fromCharCode(event.which).toLowerCase() === "s"
      ) {
        event.preventDefault();
        this.download();
      }
    });

    /* backup before closing */
    window.onbeforeunload = () => {
      this.backup().catch(this.notifyError.bind(this));
    };

    // alert when opening old Python 3.8 console
    if (this.language === "python3-old") {
      const callback = () => {
        const url = new URL(window.location.href);
        url.searchParams.set("kernel", "python3");
        window.location.href = url.toString();
      };
      this.confirm(
        "Ouvrir avec la dernière version de Basthon ?",
        "Cette console utilise une version ancienne du" +
          " noyau de Basthon (Python 3.8) qui ne sera " +
          "bientôt plus maintenue. Voulez-vous utiliser " +
          "la dernière version du noyau (Python 3.10) ?",
        "Utiliser Python 3.10",
        callback,
        "Rester avec Python 3.8",
        () => {}
      );
    }
  }

  /**
   * Run the editor script in the shell.
   */
  public async runScript() {
    await this.loaded();
    const src = this.content();
    this._shell.launch(src, false);
    this._shell.focus();
  }

  /**
   * RAZ function.
   */
  public restart() {
    this.kernelRestart();
    this._shell.clear();
    this._graphics.clean();
  }

  /**
   * Open *.py file by asking user what to do:
   * load in editor or put on (emulated) local filesystem.
   */
  public async openModuleFile(file: File) {
    this.confirm(
      "",
      `Que faire de ${file.name} ?`,
      "Charger dans l'éditeur",
      () => {
        this.open(file);
      },
      "Installer le module",
      () => {
        this.putFSRessource(file);
      }
    );
  }

  /**
   * Opening file: If it has kernel extension, loading it in the editor
   * or put on (emulated) local filesystem (user is asked to),
   * otherwise, loading it in the local filesystem.
   */
  public async openFile() {
    await this.kernelLoader.kernelAvailable();
    const callback = this.openModuleFile.bind(this);
    const callbacks: { [key: string]: (_: File) => Promise<void> } = {};
    this.kernelSafe?.moduleExts().forEach((ext: string) => {
      callbacks[ext] = callback;
    });
    return await this._openFile(callbacks);
  }

  /**
   * Displaying editor alone.
   */
  public hideConsole() {
    let button = document.getElementById("hide-console");
    if (button != null) button.style.display = "none";
    button = document.getElementById("hide-editor");
    if (button != null) button.style.display = "";
    let elt = document.getElementById("right-div");
    if (elt != null) elt.style.display = "none";
    elt = document.getElementById("left-div");
    if (elt != null) {
      elt.style.display = "";
      elt.style.width = "100%";
    }
  }

  /**
   * Displaying console alone.
   */
  public hideEditor() {
    let button = document.getElementById("hide-editor");
    if (button != null) button.style.display = "none";
    button = document.getElementById("show-editor-console");
    if (button != null) button.style.display = "";
    let elt = document.getElementById("left-div");
    if (elt != null) elt.style.display = "none";
    elt = document.getElementById("right-div");
    if (elt != null) {
      elt.style.display = "";
      elt.style.width = "100%";
    }
  }

  /**
   * Editor and console side by side.
   */
  public showEditorConsole() {
    let button = document.getElementById("show-editor-console");
    if (button != null) button.style.display = "none";
    button = document.getElementById("hide-console");
    if (button != null) button.style.display = "";
    let elt = document.getElementById("left-div");
    if (elt != null) {
      elt.style.display = "";
      elt.style.width = "50%";
    }
    elt = document.getElementById("right-div");
    if (elt != null) {
      elt.style.display = "";
      elt.style.width = "48%";
    }
  }

  /**
   * Get switch state.
   */
  private async _getSwitchState(): Promise<boolean> {
    return await this.getState("switched", false);
  }

  /**
   * Update switch view.
   */
  public async updateSwitchView() {
    const switched = await this._getSwitchState();

    let div_left = document.getElementById("left-div");
    let div_right = document.getElementById("right-div");
    if (div_left == null || div_right == null) return;

    if (switched) {
      div_left.style.cssFloat = "right";
      div_right.style.cssFloat = "left";
    } else {
      div_left.style.cssFloat = "left";
      div_right.style.cssFloat = "right";
    }
  }

  /**
   * Switching side view.
   */
  public async switchView() {
    const switched = await this._getSwitchState();
    await this.setState("switched", !switched);
    await this.updateSwitchView();
  }

  /**
   * Get darkmode.
   */
  private async _getDarkmode(): Promise<boolean> {
    return await this.getState("darkmode", true);
  }

  /**
   * Update dark/light appearence.
   */
  public async updateDarkLight() {
    const darkmode = await this._getDarkmode();
    const mode = darkmode ? "dark" : "light";
    document.documentElement.setAttribute("data-theme", mode);
    await this._editor.setTheme(darkmode);
  }

  /**
   * Switch dark/light mode.
   */
  public async switchDarkLight() {
    const darkmode = await this._getDarkmode();
    await this.setState("darkmode", !darkmode);
    await this.updateDarkLight();
  }

  /**
   * Get mode as a string (dark/light).
   */
  public async theme() {
    const darkmode = await this._getDarkmode();
    return darkmode ? "dark" : "light";
  }

  /**
   * Toggle hide/show shell/graph.
   */
  public showShell() {
    this._shell.show();
    this._graphics.hide();
  }

  /**
   * Toggle hide/show shell/graph.
   */
  public showGraph() {
    this._shell.hide();
    this._graphics.show();
  }
}
