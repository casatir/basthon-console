# Basthon-Console

**This project has moved [here](https://forge.apps.education.fr/basthon/basthon-console)**.

## Build

    npm install
    npm start

This will install dependencies then build the app and open it with your default web browser (a local web server is started).

## More info

See [Basthon-Kernel's Readme](https://framagit.org/casatir/basthon-kernel/-/blob/master/README.md#basthon).
